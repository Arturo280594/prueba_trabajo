Esta appi permite conocer si una cadena de ADN tienen diferencias genéticas 
basándose en su secuencia de ADN.

Para su ejecucion se cuenta con dos rutas:

/////////////////////////////////////////////////////////////////////////////// 
                    Ruta para verificar mutaciones
///////////////////////////////////////////////////////////////////////////////

/mutation 
Que acepta unicamente el metodo post y en el cuerpo del post la informacion 
en objeto json de la siquiente manera:

        { 
	        "dna" : ["ATGC","CATG","ATAG","TGCA"]
        }
        
Analizara los datos siemptre y cuando se cuente con un arreglo de NxN, 
de lo contrario notificara que el arreglo no es cuadrado

/////////////////////////////////////////////////////////////////////////////// 
                    Ruta para obtener estadisticas
///////////////////////////////////////////////////////////////////////////////

/stats
Que acepta unicamente el metodo get que regresa un objeto json con la 
siguiente estructura

{
    "count_mutations": 1,
    "count_no_mutation": 0,
    "ratio": 0
}

///////////////////////////////////////////////////////////////////////////////
                     Instrucciones de uso
///////////////////////////////////////////////////////////////////////////////

Para fines de practicidad se recomienda hace uso de la herramienta postman, para 
ambos casos, en los encabezados colocaremos la key: Content-Type y 
Value : "application/json".

Para el caso de /mutations el metodo sera post y en el cuerpo del metodo
la informacion a analizar como se mensiono anteriormente.


En el caso de /stats el metodo sera get y no pondremos ninguncuerpo.

Para efectos de ver su funcionalidad en linea, se anexa un link para ver el 
funcionamiento:

https://apirestfull-239119.appspot.com/